package pl.jangreg.stocks.model;


import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "stock")
public class Stock {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "stock_name")
    private String stockName;

    @Column (name = "stock_code")
    private String stockCode;

    @Column(name = "price")
    private Double price;

    @Column(name = "previous_price")
    private Double previousPrice;

    @Column(name = "stock_amount")
    private Integer stockAmount;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "stock_detail_id")
    private StockDetail stockDetail;


    @OneToOne(mappedBy = "stock")
    private Transaction transaction;


    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {CascadeType.DETACH,
                    CascadeType.MERGE,
                    CascadeType.PERSIST,
                    CascadeType.REFRESH
            })
    @JoinTable(name = "user_stock",
            joinColumns = @JoinColumn(name = "stock_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> users;


    // helper method to add users

    public void addUser (User user) {
        if (users == null) {
            users = new ArrayList<>();
        }
        users.add(user);
    }


    public Stock () {}

    public Stock(String stockName, String stockCode, Double price, Double previousPrice, Integer stockAmount, StockDetail stockDetail) {
        this.stockName = stockName;
        this.stockCode = stockCode;
        this.price = price;
        this.previousPrice = previousPrice;
        this.stockAmount = stockAmount;
        this.stockDetail = stockDetail;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPreviousPrice() {
        return previousPrice;
    }

    public void setPreviousPrice(Double previousPrice) {
        this.previousPrice = previousPrice;
    }

    public Integer getStockAmount() {
        return stockAmount;
    }

    public void setStockAmount(Integer stockAmount) {
        this.stockAmount = stockAmount;
    }

    public StockDetail getStockDetail() {
        return stockDetail;
    }

    public void setStockDetail(StockDetail stockDetail) {
        this.stockDetail = stockDetail;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "id=" + id +
                ", stockName='" + stockName + '\'' +
                ", stockCode='" + stockCode + '\'' +
                ", price=" + price +
                ", previousPrice=" + previousPrice +
                ", stockAmount=" + stockAmount +
                ", stockDetail=" + stockDetail +
                '}';
    }
}
