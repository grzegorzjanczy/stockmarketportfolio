package pl.jangreg.stocks.model;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "account")
public class Account {



    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "balance")
    private Double balance;

    @Column(name = "account_name")
    private String accountName;

    @OneToOne
    @JoinColumn (name = "user_id")
    private User userId;


    @OneToMany(mappedBy = "account", cascade = CascadeType.REMOVE)
    private List <Transaction> transactions;


    // helper method adding transactions to account


    public void addTransaction (Transaction transaction) {
        if (transactions == null) {
            transactions = new ArrayList<>();

            transactions.add(transaction);
            transaction.setAccount(this);
        }





    }

    public Account () {}

    public Account(Double balance, String accountName, User userId, List<Transaction> transactions) {
        this.balance = balance;
        this.accountName = accountName;
        this.userId = userId;
        this.transactions = transactions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getBalance() {

        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", balance=" + balance +
                ", accountName='" + accountName + '\'' +
                ", userId=" + userId +
                ", transactions=" + transactions +
                '}';
    }
}
