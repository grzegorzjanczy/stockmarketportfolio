package pl.jangreg.stocks.model;


import javax.persistence.*;

@Entity
@Table(name = "stock_detail")
public class StockDetail {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long stockId;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "company_description")
    private String companyDescription;


    @OneToOne(mappedBy = "stockDetail",
                cascade = {CascadeType.DETACH
                        ,CascadeType.MERGE
                        ,CascadeType.PERSIST
                        ,CascadeType.REFRESH})
    private Stock stock;



    public StockDetail () {}

    public StockDetail(String companyName, String companyDescription, Stock stock) {
        this.companyName = companyName;
        this.companyDescription = companyDescription;
        this.stock = stock;
    }


    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyDescription() {
        return companyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "StockDetail{" +
                "stockId=" + stockId +
                ", companyName='" + companyName + '\'' +
                ", companyDescription='" + companyDescription + '\'' +
                ", stock=" + stock +
                '}';
    }
}
